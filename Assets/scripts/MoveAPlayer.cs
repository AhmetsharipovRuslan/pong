﻿using UnityEngine;
using System.Collections;

public class MoveAPlayer : MyBehavior {
	public float speed=1f;
	public  MoveABall Ball;
	private float horizontalMovement;
	private float verticalMovement;

	public override void MoveHorizontal (float movementValue)
	{
		horizontalMovement = movementValue*speed;
	}
	
	public override void MoveVertical (float movementValue)
	{
		verticalMovement = movementValue*speed;
	}
	public override void PullABall()
	{
		if (!Ball.enabled) {
						Ball.enabled = true;
						Ball.check = true;
				}
	}

	void FixedUpdate () {
		if (rigidbody.transform.position.x > 7.5f && horizontalMovement > 0f) {
						horizontalMovement = 0f;
				} else if (rigidbody.transform.position.x < -7.5f && horizontalMovement < 0f) {	
						horizontalMovement = 0f;
				}
		if (rigidbody.transform.position.y > 6f && verticalMovement > 0f) {
						verticalMovement = 0f;
				} else if (rigidbody.transform.position.y < -6f && verticalMovement < 0f) {
						verticalMovement = 0f;
				}
		rigidbody.velocity = new Vector3(horizontalMovement,verticalMovement,0);
	}		
}
