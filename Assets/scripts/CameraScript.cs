﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public Transform target;
	public float multi=0f;

	private Vector3 startPosition;
	void Start()
	{
		startPosition = transform.position;
		}
	// FixedUpdate is called once per physical frame
	void FixedUpdate () {
		Vector3 temp = Vector3.zero;
		temp.x =startPosition.x + target.transform.position.x * multi;
		temp.y =startPosition.y + target.transform.position.y * multi;
		temp.z = transform.position.z;
		transform.position = temp;
	}
}
