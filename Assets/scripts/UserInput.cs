﻿using UnityEngine;
using System.Collections;

public class UserInput : MonoBehaviour {
	public MyBehavior UserBehavior;
	void Update () {
		UserBehavior.MoveVertical (Input.GetAxis("Vertical"));
		
		UserBehavior.MoveHorizontal(Input.GetAxis("Horizontal"));
		if (Input.GetKeyDown ("space")) {
						UserBehavior.PullABall ();
		}
	}
}
