﻿using UnityEngine;
using System.Collections;

public class WallReaction : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.name == "Ball") {
			GameObject go=GameObject.FindGameObjectWithTag("Player");	
			col.gameObject.GetComponent<MoveABall> ().enabled = false;
			col.gameObject.transform.position=go.transform.position+new Vector3(0f,0f,1.5f);
			col.gameObject.transform.parent=go.transform;
			col.gameObject.rigidbody.velocity=Vector3.zero;
			col.gameObject.GetComponent<MoveABall>().particle.Stop();
			col.gameObject.rigidbody.isKinematic=true;
				}
	}

}
