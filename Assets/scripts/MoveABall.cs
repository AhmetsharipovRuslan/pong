﻿using UnityEngine;
using System.Collections;

public class MoveABall : MonoBehaviour {
	private float speed=1f;
	private Vector3 speed3;
	public ParticleSystem particle;
	public bool check=false;
	void Update()
	{
		if (check) {
						transform.parent = null;
						particle.Play ();
						Vector3 speed3 = new Vector3 (0f, 0f, 15f);
						rigidbody.isKinematic = false;
						gameObject.rigidbody.AddForce (speed3, ForceMode.Impulse);
			check=false;
				}
	}
	void OnTriggerEnter (Collider col) {
			if (col.gameObject.name == "left") {
					speed3.x=-rigidbody.velocity.x;	
				} else if (col.gameObject.name == "UP") {
					speed3.y=-rigidbody.velocity.y;	
				} else if (col.gameObject.name == "Down") {
					speed3.y=-rigidbody.velocity.y;	
				} else if (col.gameObject.name == "Right") {
					speed3.x=-rigidbody.velocity.x;	
				}
		rigidbody.velocity = speed3;
		}
	void OnCollisionEnter (Collision col) {
				speed3 = rigidbody.velocity;
				if (col.gameObject.tag == "Player")
						speed = rigidbody.velocity.magnitude;
		}
	void OnCollisionExit (Collision col) {
		if (col.gameObject.tag == "Player") {
						speed3 = rigidbody.velocity.normalized;
						speed3 *= speed;
						rigidbody.velocity = speed3;
				}
	}
}
