﻿using UnityEngine;
using System.Collections;

public class MyBehavior : MonoBehaviour {

	public virtual void MoveHorizontal(float value)
	{
		Debug.Log ("Move Horizontal isn't implemented");
	}
	
	public virtual void MoveVertical(float value)
	{
		Debug.Log ("Move Vertical isn't implemented");
	}
	public virtual void PullABall()
	{
		Debug.Log ("Pull a ball isn't implemented");
	}
}
